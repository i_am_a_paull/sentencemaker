SentenceMaker randomly selects words from a formatted text file and uses dependencies to form semi-coherent sentences.

The easiest way to run this for the time being is to run `gradle execute`. It will bring up the old Swing interface that gives your the choice of lyrics (which is really just groups of sentences which each on a new line) or an essay (groups of sentences in the form of paragraphs).

Most of this code is pretty much as it was when I first wrote this in 2002. I only added a few things to make the code a little more comprehensible and took out the really horrible stuff from the word DB.
