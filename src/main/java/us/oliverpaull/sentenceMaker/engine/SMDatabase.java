package us.oliverpaull.sentenceMaker.engine;

import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import us.oliverpaull.sentenceMaker.common.SMTools;
import us.oliverpaull.sentenceMaker.io.SentenceMakerIO;

/**
 * SMDatabase.java Created: Thu Oct 10 19:36:43 2002
 *
 * @author A Paull
 */
public class SMDatabase {

  Map<WordType, List<Word>> wordsByType = Maps.newHashMap();

  public SMDatabase(String dbString) {
    for (WordType wordType : WordType.values()) {
      wordsByType.put(wordType, Lists.newArrayList());
    }

    StringTokenizer dbTok = new StringTokenizer(dbString, "\n");
    String line;
    String theDeps = "";

    while (dbTok.hasMoreTokens()) {
      line = dbTok.nextToken();

      if (line.charAt(0) == '#') continue;

      Word word = new Word();
      Word posessive = new Word();

      StringTokenizer lineST = new StringTokenizer(line);
      WordType wordType = WordType.getByConfigName(lineST.nextToken());
      if (wordType.equals(WordType.VERB) || wordType.equals(WordType.NOUN)) {
        word.setSingular(lineST.nextToken());
        theDeps = lineST.nextToken();
        word.setDependencies(WordDependency.getAllPossibleDependencies(theDeps));

        if (theDeps.equals("NJ")) {
          String posStr = word.getSingular() + "\'s";
          posessive.setSingular(posStr);
          posessive.setPlural(posStr);
        }
      } else if (wordType.equals(WordType.DEF_ARTICLE)
          || wordType.equals(WordType.INDEF_ARTICLE)
          || wordType.equals(WordType.PRONOUN)) {
        word.setSingular(lineST.nextToken());
        word.setPlural(lineST.nextToken());
      } else {
        word.setSingular(lineST.nextToken());
      }

      if (wordType.equals(WordType.NOUN)) {
        wordsByType.get(wordType).add(word);
        if (theDeps.equals("NJ")) {
          wordsByType.get(WordType.DEF_ARTICLE).add(posessive);
        }
      } else if (wordType.equals(WordType.VERB)) {
        wordsByType.get(wordType).add(word);
        if (!theDeps.equals("V")) {
          Word gerund = new Word(gerundify(word.getSingular()));
          wordsByType.get(WordType.GERUND).add(gerund);
          wordsByType.get(WordType.ADVERB).add(new Word(gerund.getSingular() + "ly"));
        }
      } else {
        wordsByType.get(wordType).add(word);
      }
    }
  }

  private String gerundify(String verb) {
    char thirdToLast = verb.charAt(verb.length() - 3);
    char secToLast = verb.charAt(verb.length() - 2);
    char last = verb.charAt(verb.length() - 1);

    if (!SMTools.isVowel(last)) {
      if (SMTools.isVowel(secToLast)) {
        if (!SMTools.isVowel(thirdToLast)) {
          if ((last != 'w') && (last != 'y')) {
            if (!verb.equals("listen")) {
              return verb + last + "ing";
            }
          }
        }
      }
    } else {
      if ((last == 'e') && !SMTools.isVowel(secToLast)) {
        return verb.substring(0, verb.length() - 1) + "ing";
      }
    }

    return verb + "ing";
  }

  public Word getWord(WordType wordType) {
    List<Word> words = wordsByType.get(wordType);
    return words.get((int) (Math.random() * ((double) words.size() - 0.1)));
  }

  public static void main(String[] args) {
    SentenceMakerIO smio = new SentenceMakerIO("us/oliverpaull/sentenceMaker/demo/data/wordDB.txt");
    SMDatabase test = new SMDatabase(smio.getDBString());

    Word verb = test.getWord(WordType.VERB);
    System.out.println(verb.getSingular());
    WordDependency[][] verbDeps = verb.getDependencies();
    for (int i = 0; i < verbDeps.length; i++) {
      for (int j = 0; j < verbDeps[i].length; j++) {
        System.out.print(verbDeps[i][j].getAbbreviation());
      }
      System.out.println();
    }
    System.out.println();

    Word noun = test.getWord(WordType.NOUN);
    System.out.println(noun.getSingular());
    WordDependency[][] nounDeps = noun.getDependencies();
    for (int i = 0; i < nounDeps.length; i++) {
      for (int j = 0; j < nounDeps[i].length; j++) {
        System.out.print(nounDeps[i][j].getAbbreviation());
      }
      System.out.println();
    }
    System.out.println();

    Word gerund;
    for (int i = 0; i < 20; i++) {
      gerund = test.getWord(WordType.GERUND);
      System.out.println(gerund.getSingular());
    }

    System.out.println();
  }
} // SMDatabase
