package us.oliverpaull.sentenceMaker.engine;

public class Word {

  private String singular;
  private String plural;
  private WordDependency[][] dependencies;

  public Word() {}

  public Word(String singular) {
    this(singular, null);
  }

  public Word(String singular, String plural, WordDependency[]... dependencies) {
    this.singular = singular;
    this.plural = plural;
    this.dependencies = dependencies;
  }

  public String getSingular() {
    return singular;
  }

  public void setSingular(String singular) {
    this.singular = singular;
  }

  public String getPlural() {
    return plural;
  }

  public void setPlural(String plural) {
    this.plural = plural;
  }

  public WordDependency[][] getDependencies() {
    return dependencies;
  }

  public void setDependencies(WordDependency[][] dependencies) {
    this.dependencies = dependencies;
  }
}
