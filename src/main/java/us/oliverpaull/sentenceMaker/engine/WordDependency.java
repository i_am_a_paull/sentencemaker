package us.oliverpaull.sentenceMaker.engine;

import java.util.List;

import com.google.common.collect.Lists;

public enum WordDependency {
  ADVERB('A'), // can have adverb
  DIR_OBJ('D'), // can have direct object
  INDIR_OBJ('I'), // can have indirect object
  ADJECTIVE('J'), // can have adjective
  ARTICLE('R'), // can have article
  NOTHING('N'), // can be standalone
  PREP_PHRASE('P'), // can have prepositional phrase
  HELPING_VERB('V'); // is a helping verb

  private char abbreviation;

  WordDependency(char abbrev) {
    this.abbreviation = abbrev;
  }

  public char getAbbreviation() {
    return abbreviation;
  }

  public static WordDependency getByAbbreviation(char abbrev) {
    for (WordDependency dep : WordDependency.values()) {
      if (dep.abbreviation == abbrev) return dep;
    }
    return null;
  }

  public static WordDependency[][] getAllPossibleDependencies(String modes) {
    List<WordDependency[]> allPossibleDeps = Lists.newArrayList();

    for (int i = 0; i < modes.length(); i++) {
      if (modes.substring(i, i + 1).equals("N")) {
        allPossibleDeps.add(new WordDependency[] {WordDependency.NOTHING});
      } else {
        for (int j = modes.length(); j > i; j--) {
          // System.out.println("Hey!");
          List<WordDependency> deps = Lists.newArrayList();
          for (char c : modes.substring(i, j).toCharArray()) {
            deps.add(WordDependency.getByAbbreviation(c));
          }
          allPossibleDeps.add(deps.toArray(new WordDependency[0]));
          // System.out.println("----" + dependencies.substring(i, j));
        }
      }
    }

    return allPossibleDeps.toArray(new WordDependency[0][]);
  }
}
