package us.oliverpaull.sentenceMaker.engine;

import java.net.URL;

import us.oliverpaull.sentenceMaker.common.SMTools;
import us.oliverpaull.sentenceMaker.io.SentenceMakerIO;

/**
 * SMEngine.java Created: Thu Oct 10 23:36:18 2002
 *
 * @author A Paull
 */
public class SMEngine {

  SMDatabase wordbank;
  SentenceMakerIO smio;

  public SMEngine(String dbLocation) {
    smio = new SentenceMakerIO(dbLocation);
    wordbank = new SMDatabase(smio.getDBString());
  }

  private String getPrepositionalPhrase() {
    boolean plural;

    Word prep = wordbank.getWord(WordType.PREPOSITION);
    String thePrep = prep.getSingular();

    if (thePrep.equals("between")) {
      plural = true;
    } else plural = SMTools.decideForMe();
    return thePrep + " " + getNounPhrase(plural, false);
  }

  private String getAdverb() {
    Word adverb = wordbank.getWord(WordType.ADVERB);
    return adverb.getSingular();
  }

  private String getArticle(boolean plural, String nextWord) {
    Word article;
    String theArticle;
    if (SMTools.decideForMe()) {
      article = wordbank.getWord(WordType.DEF_ARTICLE);
    } else {
      article = wordbank.getWord(WordType.INDEF_ARTICLE);
    }

    theArticle = plural ? article.getPlural() : article.getSingular();

    if (theArticle.equals("a") && SMTools.isVowel(nextWord.charAt(0))) {
      theArticle = "an";
      if (nextWord.equals("other")) {
        return theArticle + nextWord;
      }
    }
    return theArticle + " " + nextWord;
  }

  private String getAdjective() {
    Word adjective;

    if (SMTools.decideForMe()) {
      adjective = wordbank.getWord(WordType.ADJECTIVE);
    } else {
      adjective = wordbank.getWord(WordType.GERUND);
    }

    return adjective.getSingular();
  }

  private String desubjectize(String pronoun) {
    if (pronoun.equals("I")) return "me";
    else if (pronoun.equals("he")) return "him";
    else if (pronoun.equals("she")) return "her";
    else if (pronoun.equals("we")) return "us";
    else if (pronoun.equals("they")) return "them";
    else return pronoun;
  }

  private String getNounPhrase(boolean plural, boolean subject) {
    Word noun;
    String theNoun;

    if (SMTools.decideForMe()) {
      noun = wordbank.getWord(WordType.PRONOUN);
      theNoun = plural ? noun.getPlural() : noun.getSingular();

      if (!subject) {
        theNoun = desubjectize(theNoun);
      }
    } else {
      noun = wordbank.getWord(WordType.NOUN);
      theNoun = noun.getSingular();
      if (plural) theNoun = pluralize(theNoun, "noun");
      WordDependency[][] depList = noun.getDependencies();
      WordDependency[] firstDep = depList[0];
      int listIndex = (int) (Math.random() * ((double) depList.length - 0.1));

      WordDependency[] deps = depList[listIndex];
      boolean articleApplied = false;

      if (!(firstDep.length == 1 && firstDep[0].equals(WordDependency.NOTHING))
          && (deps.length == 1 && deps[0].equals(WordDependency.PREP_PHRASE))) {
        deps = new WordDependency[] {WordDependency.ARTICLE, WordDependency.PREP_PHRASE};
      }

      for (int i = 0; i < deps.length; i++) {
        if (deps[i] == WordDependency.NOTHING) {
          theNoun = theNoun;
        } else if (deps[i] == WordDependency.ADJECTIVE) {
          String theAdjective = getAdjective();
          if (!firstDep.equals("N")) {
            theAdjective = getArticle(plural, theAdjective);
            articleApplied = true;
          }
          theNoun = theAdjective + " " + theNoun;
        } else if (deps[i] == WordDependency.ARTICLE && !articleApplied) {
          theNoun = getArticle(plural, theNoun);
        } else if (deps[i] == WordDependency.PREP_PHRASE) {
          if (plural || articleApplied) {
            theNoun = theNoun + " " + getPrepositionalPhrase();
          }
        }
      }

      // System.out.println(articleApplied);
    }
    return theNoun;
  }

  private String getDecPredicate(boolean plural, boolean helpingVerb, String subject) {
    Word verb = null;
    String pred = "";
    WordDependency[][] depList = null;
    int listIndex = 0;
    WordDependency[] deps = null;

    boolean goOn = false;

    while (!goOn) {
      verb = wordbank.getWord(WordType.VERB);
      pred = verb.getSingular();
      depList = verb.getDependencies();
      listIndex = (int) (Math.random() * ((double) depList.length - 0.1));

      deps = depList[listIndex];

      if (deps.length == 1 && deps[0].equals(WordDependency.HELPING_VERB)) {
        if (!helpingVerb) {
          return pred + " " + getDecPredicate(true, true, "");
        }
      } else {
        goOn = true;
      }
    }

    if (!plural && !subject.equals("I")) {
      pred = pluralize(pred, "verb");
    }
    for (int i = 0; i < deps.length; i++) {
      if (deps[i] == WordDependency.NOTHING) {
        pred = pred;
      } else if ((deps[i] == WordDependency.INDIR_OBJ) || (deps[i] == WordDependency.DIR_OBJ)) {
        pred = pred + " " + getNounPhrase(SMTools.decideForMe(), false);
      } else if (deps[i] == WordDependency.ADVERB) {
        pred = pred + " " + getAdverb();
      } else if (deps[i] == WordDependency.PREP_PHRASE) {
        pred = pred + " " + getPrepositionalPhrase();
      }
    }

    return pred;
  }

  private String pluralize(String word, String wordType) {
    int length = word.length();

    if (wordType.equals("noun")) {
      if (word.equals("goose")) return "geese";
      if (word.equals("mouse")) return "mice";
      if (word.equals("larva") || word.equals("vertebra")) {
        return word + "e";
      }
      if (length >= 4) {
        if (word.substring(length - 4).equals("deer")) return word;
        if (word.substring(length - 2).equals("us")) {
          return word.substring(0, length - 2) + "i";
        }
      }
    }
    switch (word.charAt(length - 1)) {
      case 's':
      case 'h':
        return word + "es";
      case 'x':
        if (word.charAt(length - 2) == 'i') {
          return word.substring(0, length - 1) + "ces";
        } else return word + "es";
      case 'y':
        // case 'i':
        if ((word.charAt(length - 2) != 'o') && (word.charAt(length - 2) != 'e')) {
          return word.substring(0, length - 1) + "ies";
        }
    }

    return word + "s";
  }

  private String getExclamation() {
    String exclamation;

    if (SMTools.decideForMe()) {
      exclamation = getDeclaration();
    } else {
      exclamation = getImperative();
    }
    return exclamation.substring(0, exclamation.length() - 1) + "!";
  }

  private String getDeclaration() {
    boolean plural = SMTools.decideForMe();
    String subject = getNounPhrase(plural, true);
    String predicate = getDecPredicate(plural, false, subject);

    return subject + " " + predicate + ".";
  }

  private String getImperative() {
    return getDecPredicate(true, false, "you") + ".";
  }

  /**
   * This method constructs a semi-coherent sentence. There are four sentence types: 0-expletive,
   * 1-exclamation, 2-declarative, 3-imperative, 4-question
   *
   * @param sentenceType - type of sentence
   */
  public String getSentence(int sentenceType) {

    switch (sentenceType) {
      case 0:
        Word expletive = wordbank.getWord(WordType.EXPLETIVE);
        return capitalize(expletive.getSingular() + "!");
      case 1:
        return capitalize(getExclamation());
      case 2:
        return capitalize(getDeclaration());
      case 3:
        return capitalize(getImperative());
        // case 4:
        // return getQuestion();
      default:
        return "Balls!";
    }
  }

  private String capitalize(String sentence) {
    String firstChar = sentence.substring(0, 1).toUpperCase();
    String restOfSentence = sentence.substring(1, sentence.length());

    return firstChar + restOfSentence;
  }

  public static void main(String[] args) {
    SMEngine sme;
    URL url;

    if (args.length > 0) {
      sme = new SMEngine(args[0]);
    } else {
      sme = new SMEngine("us/oliverpaull/sentenceMaker/demo/data/wordDB.txt");
    }

    for (int i = 0; i < 10; i++) {
      System.out.println(sme.getSentence(i % 4));
    }
  }
} // SMEngine
