package us.oliverpaull.sentenceMaker.engine;

import com.google.common.base.CaseFormat;

public enum WordType {
  NOUN,
  NAME,
  VERB,
  ADJECTIVE,
  ADVERB,
  INDEF_ARTICLE,
  DEF_ARTICLE,
  PREPOSITION,
  EXPLETIVE,
  PRONOUN,
  GERUND;

  public String getConfigName() {
    return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, this.name());
  }

  public static WordType getByConfigName(String configName) {
    return WordType.valueOf(CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, configName));
  }
}
