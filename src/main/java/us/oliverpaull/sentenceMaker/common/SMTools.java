package us.oliverpaull.sentenceMaker.common;

/**
 * SMTools.java created Oct 19, 2002
 *
 * @author A Paull
 */
public class SMTools {

  public static boolean isVowel(char c) {
    switch (c) {
      case 'a':
      case 'A':
      case 'e':
      case 'E':
      case 'i':
      case 'I':
      case 'o':
      case 'O':
      case 'u':
      case 'U':
        return true;
      default:
        return false;
    }
  }

  public static boolean decideForMe() {
    int decider = (int) (Math.random() * 1.9);

    if (decider == 1) return true;
    else return false;
  }
}
