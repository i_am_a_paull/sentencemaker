package us.oliverpaull.sentenceMaker.demo.engines;

import us.oliverpaull.sentenceMaker.engine.SMEngine;

/**
 * LyricsEngine.java Created: Thu Oct 24 17:40:18 2002
 *
 * @author A Paull
 */
public class LyricsEngine extends SMEngine {

  public LyricsEngine(String dbLocation) {
    super(dbLocation);
  }

  public String getVerse() {
    int numberOfLines = 4 + (int) (Math.random() * 8.9);

    String verse = "";
    for (int i = 0; i < numberOfLines; i++) {
      int sentenceType = (int) (Math.random() * 3.9);
      verse = verse + "\n" + getSentence(sentenceType);
    }

    return verse;
  }

  public String getSong() {
    int numberOfVerses = 2 + (int) (Math.random() * 3.9);

    String song = "";
    for (int i = 0; i < numberOfVerses; i++) {
      song = song + "\n" + getVerse();
    }

    return song;
  }

  public static void main(String[] args) {
    LyricsEngine le;

    if (args.length > 0) {
      le = new LyricsEngine(args[0]);
    } else {
      le = new LyricsEngine("us/oliverpaull/sentenceMaker/demo/data/wordDB.txt");
    }
    System.out.println(le.getSong());
  }
} // LyricsEngine
