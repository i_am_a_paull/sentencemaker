package us.oliverpaull.sentenceMaker.demo.engines;

import us.oliverpaull.sentenceMaker.engine.SMEngine;

/**
 * EssayEngine.java Created: Thu Oct 24 18:17:35 2002
 *
 * @author A Paull
 */
public class EssayEngine extends SMEngine {

  public EssayEngine(String dbLocation) {
    super(dbLocation);
  }

  public String getParagraph() {
    int numberOfSentences = 3 + (int) (Math.random() * 9.9);

    String paragraph = "     ";
    for (int i = 0; i < numberOfSentences; i++) {
      int sentenceType = (int) (Math.random() * 3.9);
      paragraph = paragraph + "  " + getSentence(sentenceType);
    }

    return paragraph + "\n\n";
  }

  public String getEssay() {
    int numberOfParagraphs = 5 + (int) (Math.random() * 2.9);

    String essay = "";
    for (int i = 0; i < numberOfParagraphs; i++) {
      essay = essay + getParagraph();
    }

    return essay;
  }

  public static void main(String[] args) {
    EssayEngine ee;

    if (args.length > 0) {
      ee = new EssayEngine(args[0]);
    } else {
      ee = new EssayEngine("us/oliverpaull/sentenceMaker/demo/data/wordDB.txt");
    }
    System.out.println(ee.getEssay());
  }
} // EssayEngine
