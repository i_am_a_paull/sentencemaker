package us.oliverpaull.sentenceMaker.demo.gui.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

import us.oliverpaull.sentenceMaker.demo.engines.EssayEngine;
import us.oliverpaull.sentenceMaker.demo.engines.LyricsEngine;

public class SMDemo {

  private JFrame frame;
  private LyricsEngine le;
  private EssayEngine ee;

  private JTextArea textArea;

  public SMDemo() {
    String dbLocation = "us/oliverpaull/sentenceMaker/demo/data/wordDB.txt";
    le = new LyricsEngine(dbLocation);
    ee = new EssayEngine(dbLocation);

    initComponents();
  }

  private void initComponents() {
    // JMenu menu;
    JToolBar toolbar = new JToolBar();
    JButton lyricsButton = new JButton("Lyrics");
    JButton essayButton = new JButton("Essay");
    JButton clearButton = new JButton("Clear");
    JPanel pane = new JPanel(new BorderLayout());
    textArea = new JTextArea(5, 30);
    textArea.setWrapStyleWord(true);
    JScrollPane scrollPane = new JScrollPane(textArea);

    pane.add(scrollPane, BorderLayout.CENTER);

    ActionListener aL = new MyActionListener();
    lyricsButton.addActionListener(aL);
    essayButton.addActionListener(aL);
    clearButton.addActionListener(aL);

    toolbar.add(lyricsButton);
    toolbar.add(essayButton);
    toolbar.add(clearButton);
    pane.add(toolbar, BorderLayout.SOUTH);

    pane.setPreferredSize(new Dimension(500, 300));

    frame = new JFrame();
    WindowListener wL = new MyWindowListener();
    frame.addWindowListener(wL);
    frame.getContentPane().add(pane);
    frame.pack();
  }

  public JFrame getGUI() {
    return frame;
  }

  private class MyActionListener implements ActionListener {

    public void actionPerformed(ActionEvent e) {
      if (e.getActionCommand().equals("Lyrics")) {
        textArea.append(le.getSong());
      }
      if (e.getActionCommand().equals("Clear")) {
        textArea.setText("");
      }
      if (e.getActionCommand().equals("Essay")) {
        textArea.append(ee.getEssay());
      }
    }
  }

  private class MyWindowListener implements WindowListener {

    /**
     * Invoked when a Window is no longer the active Window. Only a Frame or a Dialog can be the
     * active Window. The native windowing system may denote the active Window or its children with
     * special decorations, such as a highlighted title bar. The active Window is always either the
     * focused Window, or the first Frame or Dialog that is an owner of the focused Window.
     */
    public void windowDeactivated(WindowEvent e) {
      // TODO
    }

    /** Invoked when a window is changed from a minimized to a normal state. */
    public void windowDeiconified(WindowEvent e) {
      // TODO
    }

    /** Invoked when a window has been closed as the result of calling dispose on the window. */
    public void windowClosed(WindowEvent e) {
      System.exit(0);
    }

    /**
     * Invoked when a window is changed from a normal to a minimized state. For many platforms, a
     * minimized window is displayed as the icon specified in the window's iconImage property.
     *
     * @see java.awt.Frame#setIconImage
     */
    public void windowIconified(WindowEvent e) {
      // TODO
    }

    /** Invoked the first time a window is made visible. */
    public void windowOpened(WindowEvent e) {
      // TODO
    }

    /**
     * Invoked when the Window is set to be the active Window. Only a Frame or a Dialog can be the
     * active Window. The native windowing system may denote the active Window or its children with
     * special decorations, such as a highlighted title bar. The active Window is always either the
     * focused Window, or the first Frame or Dialog that is an owner of the focused Window.
     */
    public void windowActivated(WindowEvent e) {
      // TODO
    }

    /**
     * Invoked when the user attempts to close the window from the window's system menu. If the
     * program does not explicitly hide or dispose the window while processing this event, the
     * window close operation will be cancelled.
     */
    public void windowClosing(WindowEvent e) {
      System.exit(0);
    }
  }

  public static void main(String[] args) {
    SMDemo smd = new SMDemo();

    smd.getGUI().show();
  }
}
