package us.oliverpaull.sentenceMaker.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * SentenceMakerIO.java Created: Fri Oct 18 14:47:30 2002
 *
 * @author A Paull
 */
public class SentenceMakerIO {

  String dbString;

  public SentenceMakerIO(String dbFile) {
    try {
      createDBString(createURL(dbFile));
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  private void createDBString(URL dbURL) {
    URLConnection conn = null;
    BufferedReader data = null;
    String line;
    StringBuffer buf = new StringBuffer();

    try {
      conn = dbURL.openConnection();
      conn.connect();

      data = new BufferedReader(new InputStreamReader(conn.getInputStream()));

      while ((line = data.readLine()) != null) {
        buf.append(line + "\n");
      }

      dbString = buf.toString();
      data.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private URL createURL(String dbFile) throws MalformedURLException {
    URL dbURL = null;

    if (dbFile.startsWith("http://")) {
      dbURL = new URL(dbFile);
    } else {
      dbURL = getClass().getClassLoader().getResource(dbFile);
      System.out.println("dbURL: " + dbURL);
    }

    return dbURL;
  }

  public String getDBString() {
    return dbString;
  }

  public static void main(String[] args) {
    SentenceMakerIO smio1 = new SentenceMakerIO("../wordbanks/test.txt");
    SentenceMakerIO smio2 = null;

    smio2 = new SentenceMakerIO("http://phildopus.headofphil.net/stuff/fromage.txt");

    System.out.println(smio1.getDBString());
    System.out.println("-------------------------------------");
    System.out.println(smio2.getDBString());
  }
} // SentenceMakerIO
