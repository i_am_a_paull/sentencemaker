# This is the database that comprises
# LyricsMaker's vocabulary. As you can
# see, this is customizable.
# 
# For instructions on how to add words,
# check the LyricsMaker help menu.
#--------------------------------------
noun		wasabi		JRP
noun		sushi		JRP
noun		sashimi		JRP
noun		Alana		NJ
noun		Lane		NJ
noun		Karen		NJ
noun		nacho		JRP
verb		scream		NAP
verb		glow		NAP
noun		saddle		JRP
noun		nut		NJRP
noun		mule		JRP
adjective	spherical
verb		seek		NDAP
preposition	for
adjective	five-pound
noun		tube		JRP
noun		kiwi		JRP
noun		gnu		JRP
noun		trashcan	JRP
noun		sword		JRP
noun		justice		NJRP
adjective	wet
adjective	sweaty
verb		exalt		NDAP
adjective	omnipresent
adjective	ineffable
adjective	convivial
verb		ferment		NAP
noun		grape		JRP
noun		stick		JRP
adjective	sticky
adjective	squishy
adjective	taciturn
adjective	uncouth
noun		youth		JRP
adjective	young
adjective	old
adjective	new
adjective	recent
adjective	nonchalant
noun		band		JRP
noun		shoe		JRP
verb		maim		NDAP
verb		injure		NDAP
verb		kill		NDAP
pronoun		it		they
pronoun		he		they
pronoun		she		they
pronoun		I		we
pronoun		that		those
pronoun		this		these
noun		something	NJRP
noun		nothing		NJRP
adjective	sweet
adjective	fair
noun		lass		JRP
noun		lad		JRP
adjective	Scottish
adjective	Irish
adjective	American
adjective	Canadian
adjective	Australian
adjective	Chinese
adjective	Japanese
adjective	Italian
noun		stallion	NJRP
preposition	in
adjective	nice
adjective	splendid
adverb		splendidly
adjective	wonderful
adverb		wonderfully
adverb		stupidly
defArticle	the		the
defArticle	this		these
defArticle	that		those
indefArticle	a		some
noun		brain		JRP
noun		phone		JRP
noun		computer	JRP
adjective	disgusting
adjective	brown
adjective	red
verb		love		NDA
expletive	balls
expletive	cock
noun		cup		JRP
noun		penguin		JRP
noun		ball		JRP
verb		drink		NDAP
verb		eat		NDAP
noun		drink		JRP
noun		beef		NJRP
noun		dragon		JRP
adjective	beefy
adjective	meaty
adjective	saucy
adjective	vapid
verb		wipe		NDAP
noun		wipe		JRP
noun		dress		JRP
verb		dress		NDAP
verb		mail		NIDAP
noun		beer		NJRP
noun		clock		JRP
noun		reindeer	JRP
verb		rub		NDAP
noun		rube		JRP
preposition	outside
preposition	atop
preposition	under
preposition	at
preposition	between
preposition	below
preposition	from
preposition	to
preposition	by
verb		throw		NIDAP
adjective	toxic
noun		bear		JRP
verb		maul		NDAP
noun		juice		JRP
verb		juice		NDAP
noun		squirrel	JRP
noun		gerbil		JRP
noun		wombat		JRP
adjective	gelatinous
adjective	incomprehensible
noun		pudding		NJRP
verb		masticate	NDAP
adjective	ephemeral
noun		syllogism	JRP
adjective	seismic
noun		discontinuity	NJRP
adjective	electromagnetic
adjective	stinky
verb		destroy		NDAP
verb		become		NDAP
verb		come		NAP
verb		can		V
verb		cannot		V
verb		will		V
verb		shall		V
adverb		languidly
adverb		randomly
adverb		jauntily
adverb		prissily
adjective	prissy
adjective	random
adjective	randy
adverb		smashingly
adjective	smashing
adjective	righteous
indefArticle	a		two
indefArticle	one		three
indefArticle	a		five
indefArticle	one		eighty
indefArticle	a		666
adjective	other
noun		other		JRP
verb		lick		NDAP
noun		mouse		JRP
noun		sausage		NJRP
preposition	like
expletive	balderdash	
defArticle	thy		thy
defArticle	my		my
defArticle	your		your
defArticle	ye		ye
verb		spackle		NDAP
noun		pant		JRP
verb		pant		NAP
noun		clown		JRP
expletive	forsooth		
verb		plunge		NDAP
noun		horse		JRP
verb		emote		NAP
adjective	pedantic	
adjective	throbbing
adjective	molten
adjective	glistening
noun		larva		JRP
adjective	summer
adjective	winter
adjective	autumn
adjective	spring
noun		squash		JRP
noun		cucumber	JRP
noun		pear		JRP
defArticle	yonder		yonder
noun		toe		JRP
noun		nose		JRP
adjective	straight
adjective	similar
adjective	large
adjective	bleak
adjective	violent
adjective	dubious
noun		trouser		JRP
verb		rip		NIDAP
verb		change		NDAP
noun		thing		JRP
noun		Pepsi		JRP
noun		Coke		JRP
noun		Sprite		JRP
noun		Fanta		JRP
noun		fan		JRP
noun		guitar		JRP
noun		lamp		JRP
noun		pump		JRP
verb		pump		NDAP
noun		example		JRP
noun		server		JRP
adjective	remote
noun		hermit		JRP
noun		recluse		JRP
noun		paper		JRP
noun		goose		JRP
noun		dinner		JRP
noun		lunch		JRP
noun		spine		JRP
adjective	severed
adjective	androgynous
verb		email		NIDAP
verb		dance		NAP
verb		screech		NAP
verb		pop		NDAP
verb		flash		NDAP
verb		tear		NDAP
verb		turn		NAP
verb		itch		NAP
verb		scratch		NDAP
verb		comb		NDAP
noun		hair		NJRP
verb		slurp		NDAP
verb		pull		NDAP
verb		hear		NDAP
verb		listen		NAP
noun		jort		JRP
noun		toot		JRP
verb		toot		NIDAP
noun		girl		JRP
noun		boy		JRP
noun		enby		JRP
noun		femme		JRP
adjective	femme
adjective	masc
noun		butch		JRP
adjective	butch
noun		cat		JRP
noun		dog		JRP
noun		yeen		JRP
noun		bird		JRP
adjective	gay
adjective	bisexual
adjective	pansexual
adjective	sapphic
adjective	lesbian
noun		lesbian		JRP
